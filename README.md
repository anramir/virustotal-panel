# \<virustotal-panel\>

## Install package dependencies

Note: This repository presupposes `node` and `npm` are already installed.

Run `npm install`

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your application locally.

## Serve Application

```
$ polymer serve
```

# Test description

Panel has been developed using Polymer 3. These packages have also been included:

- _d3-time-format_: For formatting datetimes.
- _polymer/paper-button_: Button component taken from webcomponents.org.
- _polymer/iron-icons_

## Data model

Classes which represents data model can be found in `src/model`.

- QueryResult: Represents the result of the query.
- Entity: Represents one entity found (visibility, owner, create date, description...)
- User: Represents a user (can be a owner, or be in the entity visibility list).

