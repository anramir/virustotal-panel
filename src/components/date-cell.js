import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { timeFormat } from 'd3-time-format';

class DateCell extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host .date {
          color: var(--theme-font-text-color);
        }
        :host .time {
          color: var(--theme-font-header-color);
        }
      </style>
      <div>
        <span class="date">{{dateString}}</span>
        </br>
        <span class="time">{{timeString}}</span>
      </div>
    `;
  }

  static get properties() {
    return {
      date: {
        type: Date,
        value: new Date(),
        observer: 'formatDate'
      },
      dateString: {
        type: String,
        value: ""
      },
      timeString: {
        type: String,
        value: ""
      }
    }
  }

  formatDate() {
    this.setProperties({
      dateString: timeFormat("%Y-%m-%d")(this.date),
      timeString: timeFormat("%H:%M:%S")(this.date)
    })
  }
}

customElements.define("date-cell", DateCell);