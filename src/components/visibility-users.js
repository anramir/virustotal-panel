import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';

import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';

import './user-image.js';

class VisibilityUsers extends PolymerElement {
  
  static get properties() {
    return {
      users: {
        type: Array,
        value() {
          return []
        },
        observer: 'sliceUsers'
      },
      editable: {
        type: Boolean,
        value: false,
        observer: "switchEditable"
      },
      editableIcon: {
        type: String,
        value: "visibility"
      },
      isLargerThanMax: {
        type: Boolean,
        value: false
      },
      nonDisplayedUsers: {
        type: Number,
        value: 0
      },
      slicedUsers: {
        type: Array,
        value() {
          return []
        }
      }
    }
  }

  sliceUsers() {
    const maxNusers = 3;
    this.setProperties({ 
      slicedUsers: this.users.slice(0, maxNusers),
      isLargerThanMax: this.users.length > maxNusers,
      nonDisplayedUsers: this.users.length - maxNusers
    })
  }

  switchEditable() {
    this.setProperties({ editableIcon: this.editable ? "create" : "visibility"})
  }

  static get template() {
    return html`
      <style>
        :host user-image {
          float: right;
          margin-right: -15px;
        }
        :host .users {
          background-color: var(--theme-primary-color);
          border-radius: 15px;
          line-height: var(--theme-user-image-size);
          float: left;
          box-shadow: 0 0 1px var(--theme-secondary-color);
        }
        :host .content {
          border-radius: 50%;
          background-color: var(--theme-primary-color);
          width: var(--theme-user-image-size);
          height: var(--theme-user-image-size);
          float: right;
          text-align: center;
          margin-left:15px;
          box-shadow: none;
        }
        :host .content-icon {
          border-radius: 50%;
          background-color: var(--theme-primary-color);
          width: var(--theme-user-image-size);
          height: var(--theme-user-image-size);
          box-shadow: 0 0 1px var(--theme-secondary-color);
          float: right;
          text-align: center;
          --iron-icon-height: 15px;
          --iron-icon-width: 15px;
          --iron-icon-fill-color: var(--theme-secondary-color);
          margin-right: -15px;
        }
        :host .editable-icon {
          --iron-icon-height: 15px;
          --iron-icon-width: 15px;
          --iron-icon-fill-color: var(--theme-secondary-color);
          line-height: var(--theme-user-image-size);
          width: var(--theme-user-image-size);
          height: var(--theme-user-image-size);
          float: left;
          text-align: center;
          margin-left: 15px;
        }
      </style>
      <div>
        <div class="users">
          <template is="dom-if" if=[[isLargerThanMax]]>
            <div class="content">
              <span>(+[[nonDisplayedUsers]])</span>
            </div>
          </template>
          <template is="dom-repeat" items="{{slicedUsers}}">
            <user-image user="{{item}}">
            </user-image>
          </template>
          <div class="content-icon">
            <iron-icon icon="lock-outline"> </iron-icon>
          </div>
        </div>
        <div class="editable-icon">
          <iron-icon icon="[[editableIcon]]"> </iron-icon>
        </div>
      </div>
    `
  }
}

customElements.define('visibility-users', VisibilityUsers);