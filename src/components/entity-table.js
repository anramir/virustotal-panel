import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-button/paper-button.js';


import './visibility-users.js';
import './user-image.js';
import './date-cell.js';
import '../shared-styles/table-styles.js';


/**
 * EntityTable component
 * 
 * Table of entities
 */
class EntityTable extends PolymerElement {

  static get properties() {
    return {
      entities: {
        type: Array,
        value() {
          return [];
        }
      }
    }
  }

  static get template() {
    return html`
      <style include="table-style-element">
        :host table {
          table-layout: fixed;
          width: 100%;
        }
        :host .visibility-column {
          width: 180px;
        }
        :host .owner-column {
          width: 40px;
        }
        :host .date-column {
          width: 80px;
        }
        :host .buttons-column  {
          width: 80px;
        }
        :host td.buttons-column {
          text-align: right;
        }
        :host .open-button {
          font-weight: 600;
        }
      </style>
      <table>
        ${this.header}
        ${this.body}
      </table>
    `
  }

  clickHandler() {
    alert("You clicked open button!")
  }

  // Returns table header
  static get header() {
    return html`
      <thead>
        <tr>
          <th class="visibility-column">Visibility / Shared</th>
          <th class="owner-column">Owner</th>
          <th class="date-column">Creation date</th>
          <th>Description</th>
          <th class="buttons-column"></th>
        </tr>
      </thead>
    `;
  }

  static get body() {
    return html`
      <tbody>
        <template is="dom-repeat" items="{{entities}}">
          <tr>
            <td>
              <visibility-users users="[[item.visibilityUsers]]" editable="[[item.editable]]"> </visibility-users>
            </td>
            <td>
              <user-image user="[[item.owner]]" disabled="true">
              </user-image>
            </td>
            <td><date-cell date={{item.creationDate}}> </date-cell></td>
            <td>{{item.description}}</td>
            <td class="buttons-column">
              <paper-button on-click="clickHandler" class="open-button">Open</paper-button>
            </td>
          </tr>
        </template>
      </tbody>
    `;
  }
}

customElements.define('entity-table', EntityTable);