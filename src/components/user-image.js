import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';

import '../model/user.js';
import User from '../model/user.js';

/**
 * 
 */
class UserImage extends PolymerElement {

  static get properties() {
    return {
      user: {
        type: Object,
        value() {
          return User.empty
        }
      }
    }
  }

  static get template() {
    return html`
      <style>
        :host img {
          border-radius: 50%;
          box-shadow: 0 0 1px var(--theme-secondary-color);
          width: var(--theme-user-image-size);
          height: var(--theme-user-image-size);
        }
        :host {
          height: var(--theme-user-image-size);
        }
      </style>
      <img src="[[user.imagePath]]"> </img>
    `;
  }
}

customElements.define("user-image", UserImage);