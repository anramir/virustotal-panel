import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-button/paper-button.js';

import '../shared-styles/table-styles.js';

/**
 * QueryPanel component
 * 
 * Component where query text is displayed
 */
class QueryPanel extends PolymerElement {

  static get properties() {
    return {
      query: {
        type: String,
        value: ""
      }
    }
  }

  clickAddHandler() {
    alert("You clicked add button!")
  }

  clickStartNewHandler() {
    alert("You clicked start new button!")
  }

  static get template() {
    return html`
      <style include="table-style-element">
        paper-button {
          font-weight: 600;
        }
        paper-button.add-button {
          color: black;
        }
        paper-button.start-button {
          color: #394eff;
        }
        div.buttons-column {
          text-align: right;
          width: 100%;
        }
      </style>
      <table>
        <thead>
          <tr>
            <th>
              Entity search results
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              [[query]]
            </td>
            <td>
              <div class="buttons-column">
                <paper-button on-click="clickAddHandler" class="add-button"> Add </paper-button>
                <paper-button on-click="clickStartNewHandler" class="start-button"> Start new</paper-button>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    ` 
  }
}

customElements.define('query-panel', QueryPanel);
