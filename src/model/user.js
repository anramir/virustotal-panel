/**
 * User class
 * 
 * Represents all the information about a user. In this case, only image is used. Maybe name
 * could be displayed while hover.
 */
class User {
  /**
   * User class constructor
   * @param {String} name User name
   * @param {String} imagePath Path to user image
   */
  constructor(name, imagePath) {
    this.name = name;
    this.imagePath = imagePath;
  }

  /**
   * Empty user generator.
   */
  static get empty() {
    return new User("", "");
  }
};

export default User;

