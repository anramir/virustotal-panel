/**
 * QueryResult class
 * 
 * Represent the result of a query
 */
class QueryResult {
  /**
   * 
   * @param {String} query Query text
   * @param {Array[Entity]} entitiesTypeA Array of entities of type A
   * @param {Array[Entity]} entitiesTypeB Array of entities of type B
   */
  constructor(query, entitiesTypeA, entitiesTypeB) {
    this.query = query;
    this.entitiesTypeA = entitiesTypeA;
    this.entitiesTypeB = entitiesTypeB;
  }

  static get empty() {
    return new QueryResult("", [], []);
  }
}

export default QueryResult;