/**
 * Entity class
 * 
 * Represents one entity found as a result of a query.
 */
class Entity {
  /**
   * 
   * @param {Array[Users]} visibilityUsers
   * @param {Boolean} editable
   * @param {User} owner 
   * @param {Date} creationDate 
   * @param {String} description 
   */
  constructor(visibilityUsers, editable, owner, creationDate, description) {
    this.visibilityUsers = visibilityUsers;
    this.editable = editable;
    this.owner = owner;
    this.creationDate = creationDate;
    this.description = description;
  }
}

export default Entity;
