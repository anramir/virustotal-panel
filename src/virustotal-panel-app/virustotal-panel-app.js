import {
  html,
  PolymerElement
} from '@polymer/polymer/polymer-element.js';

import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/paper-button/paper-button.js';

// Import components
import '../components/query-panel.js';
import '../components/entity-table.js';

// Import data model
import User from '../model/user.js';
import Entity from '../model/entity.js';
import QueryResult from '../model/queryResult.js';

/**
 * @customElement
 * @polymer
 */
class VirustotalPanelApp extends PolymerElement {

  static get properties() {
    return {
      queryResult: {
        type: Object,
        value() {
          return new QueryResult(
            "IP Results for 1.1.1.1",

            [
              new Entity(
                [
                  new User("user", "images/avatar2.png"),
                  new User("user", "images/avatar1.png"),
                  new User("user", "images/avatar6.png"),
                  new User("user", "images/avatar5.png"),
                  new User("user", "images/img_avatar.png"),
                ],
                false,
                new User("Andrés", "images/avatar1.png"),
                new Date(),
                "Entity B description")
            ],
            [
              new Entity(
                [
                  new User("user", "images/img_avatar.png"),
                  new User("user", "images/img_avatar2.png"),
                  new User("user", "images/avatar2.png"),
                  new User("user", "images/avatar6.png"),
                  new User("user", "images/avatar1.png"),
                  new User("user", "images/avatar5.png"),
                ], 
                true,
                new User("Juan", "images/img_avatar2.png"),
                new Date(),
                "WannaCry"),
              new Entity(
                [
                  new User("user", "images/img_avatar.png"),
                  new User("user", "images/img_avatar2.png"),
                  new User("user", "images/avatar2.png"),
                  new User("user", "images/avatar6.png"),
                  new User("user", "images/avatar1.png"),
                  new User("user", "images/avatar5.png"),
                ], 
                true,
                new User("Miguel", "images/avatar5.png"),
                new Date(), "Entity A description"),
            ]
          )
        }
      }
    };
  }

  clickHandler() {
    alert("You clicked main button!")
  }

  static get template() {
    return html `
      <style>
        :host {
          --theme-font-family: 'ForoSans', Helvetica, sans-serif;
          --theme-font-header-color: #959595;
          --theme-font-text-color: #4A4A4A;
          --theme-primary-color: #F5F3F3;
          --theme-secondary-color: #959595;
          --theme-user-image-size: 30px;

          display: block;
          font-family: var(--theme-font-family);
          font-size: 12px;
          box-shadow: 0 0 1px var(--theme-secondary-color);
          background-color: var(--theme-primary-color);
          padding: 8px;
        }
        div .result {
          padding-bottom: 10px;
        }
        div .foot {
          width: 100%;
          text-align: center;
          padding: 10px;
        }
        paper-button.custom {
          background-color: #394eff;
          color: white;
          border-radius: 4px;
          text-transform: none;
          text-decoration: none;
          padding: 8px 16px;
          font-weight: 600;
        }
      </style>
      <div>
        <div class="result">
          <query-panel query="[[queryResult.query]]"> </query-panel>
          <entity-table entities="[[queryResult.entitiesTypeA]]"></entity-table>
          <entity-table entities="[[queryResult.entitiesTypeB]]"></entity-table>
        </div>
        <div class="foot">
          <paper-button on-click="clickHandler" class="custom" noink>View Graph Search details</paper-button>
        </div>
      </div>
    `;
  }

}

customElements.define('virustotal-panel-app', VirustotalPanelApp);