/**
 * This is the way defined in Polymer documentation
 * to create shared styles.
 * 
 * @see https://www.polymer-project.org/3.0/docs/devguide/style-shadow-dom
 */

const tableStyle = document.createElement("dom-module");

tableStyle.innerHTML = `
  <template>
    <style>
      :host th, td {
        padding: 10px;
      }
      :host thead {
        color: var(--theme-font-header-color);
        background: var(--theme-primary-color);
        text-align: left;
      }
      :host tbody {
        color: var(--theme-font-text-color);
        background-color: white;
      }
      :host tr {
        border-bottom: solid 2px var(--theme-primary-color);
      }
      :host table {
        border-collapse: collapse;
        width: 100%;
      }
    </style>
  </template>
`;

tableStyle.register("table-style-element");